package com.elestopadov;

import org.junit.Test;
import org.junit.Before;

import static org.junit.Assert.*;

public class CalculatorTest {

    private static Calculator calculator;

    @Before
    public void setUp() {
        calculator = new Calculator();
    }

    @Test
    public void whenCalculatorInitializedThenReturnTrue() {
        assertTrue(calculator.getStatus());
    }

    @Test
    public void whenAdditionTwoNumberThenReturnCorrectAnswer() {
        assertEquals(11, calculator.addition(5, 6));
    }

    @Test
    public void whenSubtractionThenReturnCorrectAnswer() {
        assertEquals(4, calculator.subtraction(7, 3));
    }

    @Test
    public void whenDivisionThenReturnCorrectAnswer() {
        assertEquals(2, calculator.division(10, 5));
    }

}
