package com.elestopadov;

public class Calculator {

    private boolean status;

    public Calculator() {
        this.status = true;
    }

    public int addition(int a, int b) {
        return a + b;
    }

    public int subtraction(int a, int b) {
        return a - b;
    }

    public int division(int a, int b) {
        if (b == 0) {
            throw new IllegalArgumentException("Can't division by zero");
        } else {
            return a / b;
        }
    }

    public boolean getStatus() {
        return status;
    }

    public static void main(String[] args){
        Calculator calculator = new Calculator();
        System.out.println("This is the console calculator");
        System.out.println(calculator.addition(100500, 600));
    }
}
